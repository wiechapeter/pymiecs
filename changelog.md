# changelog


## [v0.4] - 2024-12-13
 - added materials ZrO2 and TiO2 (average or specific birefringent axis)

 
## [v0.3] - 2024-12-09
 - Q output format change: no longer use empty dimensions for averaged results

